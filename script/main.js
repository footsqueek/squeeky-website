//Global Proxy Variables
var naoSession;
var textToSpeech;
var behaviorManager;
var posture;
var naoSystem;
var battery;
var motion;
var photoCapture;
var nextImgNo;
var aniSay;
var sound;
var life;
var lastImgNo;
var video;

$(document).ready(function () {

	//-------------------SIDR-------------------

	$('#simple-menu').sidr();


	//------------SIDR NAVIGATION EVENTS------------

	$("#link-home").click(function () {

		$("#link-dialog").removeClass("active");
		$("#link-commands").removeClass("active");
		$("#link-home").addClass("active");
		$("#link-photos").removeClass("active");
		$("#link-settings").removeClass("active");
		$("#link-movement").removeClass("active");
		$("#link-chore").removeClass("active");

	});

	$("#link-dialog").click(function () {

		$("#link-home").removeClass("active");
		$("#link-commands").removeClass("active");
		$("#link-dialog").addClass("active");
		$("#link-photos").removeClass("active");
		$("#link-settings").removeClass("active");
		$("#link-movement").removeClass("active");
		$("#link-chore").removeClass("active");

	});

	$("#link-commands").click(function () {

		$("#link-commands").addClass("active");

		$("#link-home").removeClass("active");

		$("#link-dialog").removeClass("active");

		$("#link-photos").removeClass("active");

		$("#link-settings").removeClass("active");

		$("#link-movement").removeClass("active");

		$("#link-chore").removeClass("active");

	});

	$("#link-photos").click(function () {

		$("#link-photos").addClass("active");

		$("#link-home").removeClass("active");

		$("#link-dialog").removeClass("active");

		$("#link-commands").removeClass("active");

		$("#link-settings").removeClass("active");

		$("#link-movement").removeClass("active");
		$("#link-chore").removeClass("active");

	});

	$("#link-settings").click(function () {

		$("#link-settings").addClass("active");

		$("#link-home").removeClass("active");

		$("#link-dialog").removeClass("active");

		$("#link-commands").removeClass("active");

		$("#link-photos").removeClass("active");

		$("#link-movement").removeClass("active");
		$("#link-chore").removeClass("active");

	});

	$("#link-movement").click(function () {

		$("#link-movement").addClass("active");

		$("#link-home").removeClass("active");

		$("#link-dialog").removeClass("active");

		$("#link-commands").removeClass("active");

		$("#link-photos").removeClass("active");

		$("#link-settings").removeClass("active");
		$("#link-chore").removeClass("active");

	});

	$("#link-chore").click(function () {

		$("#link-chore").addClass("active");

		$("#link-home").removeClass("active");

		$("#link-dialog").removeClass("active");

		$("#link-commands").removeClass("active");

		$("#link-photos").removeClass("active");

		$("#link-settings").removeClass("active");
		$("#link-movement").removeClass("active");

	});

	$("#btn-shutdown").click(function () {
		posture.goToPosture("Crouch", 0.5);
		naoSystem.shutdown();

	});

	$("#btn-reboot").click(function () {

		posture.goToPosture("Crouch", 0.5);
		naoSystem.reboot();

	});

	//Apply knockout.js bindings
	ko.applyBindings(new navViewModel());

});

//---------------END OF DOC READY---------------

//-------------QIMessaging Initialisation-------------
function startSession(ipaddress) {

	if (ipaddress == null) {
		naoSession = new QiSession();
	} else {
		naoSession = new QiSession(ipaddress);
	}

	naoSession.socket().on('connect', function () {
		//Initialise Proxies here

		initTextToSpeech();
		initBehaviorManager();
		initPosture();
		initSystem();
		initBattery();
		initMemory();
		initMotion();
		initCamera();
		initAniSay();
		initSound();
		initAuto();

		$("#lbl-connection").text("Connected");

	}).on('disconnect', function () {

		$("#lbl-connection").text("Not Connected");
		$("#lbl-name").text("N/A");
		$("#lbl-battery").text("N/A");
		$("lbl-charging").text("");

	});

}

function initMemory() {

	naoSession.service("ALMemory").done(function (ALMemory) {
		ALMemory.subscriber("BatteryChargeChanged").done(function (subscriber) {

			subscriber.signal.connect(function (level) {
				$("#lbl-battery").text(level + "%");
			});
		});

		ALMemory.subscriber("BatteryChargingFlagChanged").done(function (chargingSub) {

			chargingSub.signal.connect(function (state) {
				console.log(state);
				if (state) {
					$("#lbl-charging").text("(Charging)");
				} else {
					$("#lbl-charging").text("");
				}


			});

		});
	});



}

function initCamera() {

	naoSession.service("ALPhotoCapture").done(function (photo) {

		photoCapture = photo;
		photoCapture.setResolution(3);
		photoCapture.setPictureFormat("jpg");

	}).fail(function (error) {

		console.log("An error occurred:", error);

	});

	naoSession.service("ALVideoRecorder").done(function (vid) {

		video = vid;
		video.setResolution(2);
		video.setFrameRate(15);
		video.setVideoFormat("MJPG");

	}).fail(function (error) {
		console.log("An error occurred: " + error);
	});


}

function initTextToSpeech() {

	naoSession.service("ALTextToSpeech").done(function (tts) {

		textToSpeech = tts;
		//tts.say("I'm now connected through H T T P");

	}).fail(function (error) {

		console.log("An error occurred:", error);

	});

}

function sayText(speech) {

	textToSpeech.say(speech);

}

function initBehaviorManager() {

	naoSession.service("ALBehaviorManager").done(function (bm) {

		behaviorManager = bm;

		behaviorManager.getSystemBehaviorNames().done(function (behaviors) {

			console.log(behaviors);

		});



	}).fail(function (error) {

		console.log("An error occurred:", error);

	});


}

function initPosture() {

	naoSession.service("ALRobotPosture").done(function (rp) {

		posture = rp;

	}).fail(function (error) {

		console.log("An error occurred:", error);

	});

}

function initSystem() {

	naoSession.service("ALSystem").done(function (sys) {

		naoSystem = sys;

		naoSystem.robotName().done(function (name) {

			$("#lbl-name").text(name);

		});

	}).fail(function (error) {

		console.log("An error occurred:", error);

	});

}

function initMotion() {


	naoSession.service("ALMotion").done(function (mot) {

		motion = mot;

	}).fail(function (error) {

		console.log("An error occurred:", error);

	});

}

function initBattery() {

	naoSession.service("ALBattery").done(function (batt) {

		battery = batt;

		getBatteryLevel();

	}).fail(function (error) {
		console.log("An error occurred: " + error);
	});

}

function initAniSay() {

	naoSession.service("ALAnimatedSpeech").done(function (ani) {

		aniSay = ani;

	}).fail(function (error) {
		console.log("An error occurred: " + error);
	});


}

function initSound() {

	naoSession.service("ALAudioPlayer").done(function (audio) {

		sound = audio;

	}).fail(function (error) {
		console.log("An error occurred: " + error);
	});


}

function initAuto() {

	naoSession.service("ALAutonomousLife").done(function (al) {

		life = al;

	}).fail(function (error) {
		console.log("An error occurred: " + error);
	});

}

function getBatteryLevel() {

	battery.getBatteryCharge().done(function (level) {
		$("#lbl-battery").text(level + "%");
	});

}

function getFtpImgs(username, password, ipaddress) {

	console.log(username, password, ipaddress);

	$("#photo-frame").empty();

	var x = 1;

	if (username == null || password == null || ipaddress == null) {

		alert("Please enter IP Address and FTP credentials to view photos");

	} else {

		loadImgs(1, username, password, ipaddress);

	}

}

function loadImgs(imgNo, username, password, ipaddress) {



	var img = new Image();

	imgUrl = "ftp://" + username + ":" + password + "@" + ipaddress + "/recordings/cameras/image";

	imgPath = imgUrl + imgNo + ".jpg";

	$('<img class="three columns background" src="' + imgPath + '"/>').load(function () {
		lastImgNo = imgNo;
		$(this).appendTo('#photo-frame');
		loadImgs(imgNo + 1, username, password, ipaddress);
	});

}

function say(speech) {

	textToSpeech.say(speech);

}

//------KO JS Single Page Application code-----------
function navViewModel() {

	var self = this;

	self.pageHome = ko.observable(true);
	self.pageBehaviors = ko.observable(null);
	self.pageDialog = ko.observable(null);
	self.pagePictures = ko.observable(null);
	self.pageSettings = ko.observable(null);
	self.pageMove = ko.observable(null);
	self.pageChore = ko.observable(null);

	self.choreStage = ko.observable("");
	
	self.voiceShaping = ko.observable(100);
	self.voiceSpeed = ko.observable(100);

	self.speakText = ko.observable();
	self.choreDialog = ko.observable();
	self.choreBehavior = ko.observable();

	self.ipAddress = ko.observable("10.0.1.4");

	self.ftpUsername = ko.observable("nao");
	self.ftpPass = ko.observable("Levram6852");

	self.left = ko.observable(1);
	self.right = ko.observable(1);
	self.forwards = ko.observable(1);
	self.backwards = ko.observable(1);

	self.acw = ko.observable(90);
	self.cw = ko.observable(90);
	self.zero = ko.observable(true);
	self.one = ko.observable(true);
	self.two = ko.observable(true);
	self.three = ko.observable(true);
	self.four = ko.observable(true);
	self.five = ko.observable(true);
	self.six = ko.observable(true);
	self.seven = ko.observable(true);
	self.eight = ko.observable(true);
	self.nine = ko.observable(true);
	self.ten = ko.observable(true);
	self.eleven = ko.observable(true);
	self.twelve = ko.observable(true);
	self.thirteen = ko.observable(true);
	self.fourteen = ko.observable(true);


	self.clickCamera = function () {

		console.log("Picture" + lastImgNo);
		photoCapture.takePicture("/home/nao/recordings/cameras/", "image" + (lastImgNo + 1));
		loadImgs(lastImgNo + 1);
	}

	self.clickVideo = function (operation) {

		if (operation == "Start") {

			video.startRecording("/home/nao/recordings/cameras/", "video", true);

		} else if (operation == "Stop") {

			video.stopRecording();

		}

	}

	self.goToPage = function (page) {

		switch (page) {

		case 0:
			self.pageHome(true);
			self.pageBehaviors(null);
			self.pageDialog(null);
			self.pageMove(null);
			self.pagePictures(null);
			self.pageSettings(null);
			self.pageChore(null);
			break;
		case 1:
			self.pageHome(null);
			self.pageBehaviors(true);
			self.pageMove(null);
			self.pageDialog(null);
			self.pagePictures(null);
			self.pageSettings(null);
			self.pageChore(null);
			break;
		case 2:
			self.pageHome(null);
			self.pageBehaviors(null);
			self.pageMove(null);
			self.pageDialog(true);
			self.pagePictures(null);
			self.pageSettings(null);
			self.pageChore(null);
			break;
		case 3:
			self.pageHome(null);
			self.pageBehaviors(null);
			self.pageMove(null);
			self.pageDialog(null);
			self.pagePictures(true);
			self.pageSettings(null);
			self.pageChore(null);
			getFtpImgs(self.ftpUsername(), self.ftpPass(), self.ipAddress());
			break;
		case 4:
			self.pageHome(null);
			self.pageBehaviors(null);
			self.pageMove(null);
			self.pageDialog(null);
			self.pagePictures(null);
			self.pageSettings(true);
			self.pageChore(null);
			break;
		case 5:
			self.pageHome(null);
			self.pageBehaviors(null);
			self.pageMove(true);
			self.pageDialog(null);
			self.pagePictures(null);
			self.pageSettings(null);
			self.pageChore(null);
			break;
		case 6:
			self.pageHome(null);
			self.pageBehaviors(null);
			self.pageMove(null);
			self.pageDialog(null);
			self.pagePictures(null);
			self.pageSettings(null);
			self.pageChore(true);
			break;

		}

	}

	//--------------DIALOG-------------------
	self.clickSay = function (text) {

		console.log(text);



		var speech = "\\RST\\\\VCT=" + self.voiceShaping() + "\\\\RSPD=" + self.voiceSpeed() + "\\";


		if (text == null) {

			speech += self.speakText();
			console.log(speech);
			textToSpeech.say(speech);

		} else {

			speech += text;

			textToSpeech.say(speech);

		}

	}

	//--------------BEHAVIORS----------------
	self.clickPostures = function (position) {

		posture.goToPosture(position, 0.5);

	}

	self.clickBehaviors = function (behaviorPath) {

		behaviorManager.stopAllBehaviors();
		behaviorManager.runBehavior(behaviorPath);

	}

	self.clickStopBehaviors = function () {

		behaviorManager.stopAllBehaviors();

	}

	//--------------MOVEMENT------------------

	self.clickMove = function (direction) {

		motion.moveInit().done(function () {

			switch (direction) {

			case 0:

				motion.moveTo(parseFloat(self.forwards()), 0, 0);

				break;

			case 1:

				motion.moveTo(parseFloat(-(self.backwards())), 0, 0);

				break;

			case 2:

				motion.moveTo(0, parseFloat(self.left()), 0);
				break;

			case 3:

				motion.moveTo(0, parseFloat(-(self.right())), 0);
				break;

			case 4:
				var rads = (self.cw() * Math.PI) / 180;
				motion.moveTo(0, 0, -rads);

				break;

			case 5:
				var rads = (self.acw() * Math.PI) / 180;
				motion.moveTo(0, 0, rads);

				break;

			case 6:
				motion.stopMove();
				break;
			case 7:
				motion.rest();
				break;
			case 8:
				motion.wakeUp();
				break;

			}

		});


	}

	//--------------SETTINGS------------------
	self.clickDisconnect = function () {

		naoSession.disconnect();

	}

	self.clickConnect = function () {

		try {

			startSession(self.ipAddress());

		} catch (error) {

			alert("Could not connect to IP address: " + self.ipAddress() + ". " + error);

		}

	}

	self.clickAniSay = function (speech, animation) {
		var tts = speech;

		switch (speech) {

		case 0:
			tts = "Hello everybody";
			self.zero(null);
			break;
		case 1:
			tts = "Each year, the students union, reckonizes an idividual, who has gone, ABOVE AND BEYOND, in the classroom to captivate students...";
			self.one(null);
			break;
		case 2:
			tts = "Steve... are you ready for me to announce the nominations?";
			self.two(null);
			break;
		case 3:
			tts = "The first nominee is... Doctor Niel Pickles";
			self.three(null);
			break;
		case 4:
			tts = "The second nominee is... Heather MacDonald";
			self.four(null);
			break;
		case 5:
			tts = "The third nominee is... Tom Williams";
			self.five(null);
			break;
		case 6:
			tts = "The fourth nominee is... Doctor Katherine Harrison";
			self.six(null);
			break;
		case 7:
			tts = "and the fifth and final nominee is... Connie Hancock";
			self.seven(null);
			break;
		case 8:
			tts = "Steve... what qualities does this individual need to have? in order to win this award";
			self.eight(null);
			break;
		case 9:
			tts = "Come on steve! I really want to know who's won";
			self.nine(null);
			break;
		case 10:
			tts = "yes please Steve!";
			self.ten(null);
			break;
		case 11:
			tts = "Wow! what a deserving winner!";
			self.eleven(null);
			break;
		case 12:
			tts = "And the winner of Above and Beyond's, most innovative lecturer 2015...";
			self.twelve(null);
			break;
		case 13:
			tts = "Connie Hancock!";
			self.thirteen(null);
			break;
		case 14:
			tts = "Steve I want to shake the winners hand as well!";
			self.fourteen(null);
			break;
		}

		aniSay.say("^start(" + animation + ") \\RST\\\\VCT=120\\\\RSPD=80\\" + tts + "^wait(" + animation + ")");

	}

	self.clickPlaySound = function (filepath) {

		sound.playFile(filepath);

	}

	self.clickLife = function (state) {

		life.setState(state);

	}

	self.clickAddDialog = function () {

		
		self.choreStage(self.choreStage() + "<button onClick=\" textToSpeech.say('" + self.choreDialog() + "')\">" + self.choreDialog() + "</button>");
		
		//$("<button onClick=\" textToSpeech.say('" + self.choreDialog() + "')\">" + self.choreDialog() + "</button>").appendTo("#chore-stage");

	}

	self.clickAddBehavior = function () {
		
self.choreStage(self.choreStage() + "<button onClick=\"behaviorManager.runBehavior('" + self.choreBehavior() + "')\">" + self.choreBehavior() + "</button>");
		
		//$("<button onClick=\" behaviorManager.runBehavior('" + self.choreBehavior() + "')\">" + self.choreBehavior() + "</button>").appendTo("#chore-stage");

	}

	self.clickSaveChore = function () {

		saveTextAsFile($("#txtFileName").val());

	}
	
	self.clickLoadChore = function(){
	
		document.getElementById("fileToLoad").click();
		//loadFileAsText();
	
	}
	
	self.clickClearChore = function(){
	
	
		$("#chore-stage").empty();
	}

}


function saveTextAsFile(filename) {

	var textToWrite = $("#chore-stage").html();
	var textFileAsBlob = new Blob([textToWrite], {
		type: 'text/plain'
	});
	var fileNameToSaveAs = filename;

	var downloadLink = document.createElement("a");
	downloadLink.download = fileNameToSaveAs;
	downloadLink.innerHTML = "Download File";
	if (window.webkitURL != null) {
		// Chrome allows the link to be clicked
		// without actually adding it to the DOM.
		downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
	} else {

		// Firefox requires the link to be added to the DOM
		// before it can be clicked.

		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
		downloadLink.onclick = destroyClickedElement;
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);

	}

	downloadLink.click();

}

function destroyClickedElement(event) {

	document.body.removeChild(event.target);

}

function loadFileAsText() {
	
	var fileToLoad = document.getElementById("fileToLoad").files[0];
	
	var fileReader = new FileReader();
	fileReader.onload = function (fileLoadedEvent) {
		$("#chore-stage").empty();
		var textFromFileLoaded = fileLoadedEvent.target.result;
		$(textFromFileLoaded).appendTo("#chore-stage");
	};
	fileReader.readAsText(fileToLoad, "UTF-8");
}

function loadFile(obj) {
	
	$("#chore-stage").empty();
	
	$(obj.value).appendTo("#chore-stage");
	
	/*
	var fileToLoad = document.getElementById("fileToLoad").files[0];
	
	var fileReader = new FileReader();
	fileReader.onload = function (fileLoadedEvent) {
		$("#chore-stage").empty();
		var textFromFileLoaded = fileLoadedEvent.target.result;
		$(textFromFileLoaded).appendTo("#chore-stage");
	};
	fileReader.readAsText(fileToLoad, "UTF-8");*/
}